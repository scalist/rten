﻿using System;
using System.Timers;

namespace RtenCore
{
    class Install
    {
        #region Static Resources
        private static readonly string programName = Utilities.UJson.PullProjectName();
        private static readonly string downloadURL = Utilities.UJson.PullDownloadURL();
        private static readonly int hoursPerDownload = Utilities.UJson.PullHoursPerDownload();
        private static readonly bool shouldAutoDelete = Utilities.UJson.PullShouldAutoDelete();
        private static readonly string zipPath = string.Format("{0}{1}_ToInstall", AppDomain.CurrentDomain.BaseDirectory, programName);
        #endregion

        #region Installation (includes Dynamic Resources)
        public static void StartInstall()
        {
            #region Dynamic Resources (could be next to Static Resources but adds more complication)
            int downloadTally = Utilities.UJson.PullDownloadTally();
            string filePath = string.Format("{0}../{1}_{2}", AppDomain.CurrentDomain.BaseDirectory, programName, downloadTally.ToString());
            #endregion

            Console.WriteLine("NOTE: Attempting to start installation..");

            try
            {
                if(programName == "Rten")
                    Console.WriteLine("WARN: The AppConfig.json has not been changed! Please refer to 'https://gitlab.com/scalist/rten/wikis/appconfig' for how to configure it correctly.");

                Console.WriteLine(string.Format("UPDATE: Downloading {0}..", programName));
                Utilities.UFile.DownloadFromURL(downloadURL, zipPath);

                Console.WriteLine(string.Format("UPDATE: Installing {0}..", programName));
                Utilities.UFile.ExtractZip(zipPath, filePath);

                Console.WriteLine(string.Format("UPDATE: Cleaning {0}..", programName));
                Utilities.UFile.DeleteSpecifiedFile(zipPath);

                Console.WriteLine(string.Format("UPDATE: Completed installation of {0}!", programName));

                if (shouldAutoDelete)
                {
                    Console.WriteLine(string.Format("UPDATE: Cleaning old directory..", programName));

                    int downloadTallyForDeletion = downloadTally - 1;
                    Utilities.UFile.DeleteWholeDir(string.Format("{0}..\\{1}_{2}", AppDomain.CurrentDomain.BaseDirectory, programName, downloadTallyForDeletion.ToString()));
                }
                else
                    Console.WriteLine("NOTE: Not deleting old directory as json config has declined");

                
                Utilities.UJson.PushDownloadTally();
                downloadTally = Utilities.UJson.PullDownloadTally();
                Console.WriteLine(string.Format("UPDATE: Completed installation sucsessfully, you can find the newest install in '{0}_{1}'!", programName, downloadTally - 1));
            }
            catch(Exception ex)
            {
                FatalError(ex);
            }
        }
        #endregion

        #region Timer
        public static void InstallTimer()
        {
            Timer installTime = new Timer();
            installTime.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            installTime.Interval = hoursPerDownload * 60 * 60 * 1000;
            installTime.Enabled = true;

            Console.ReadKey();
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            StartInstall();
        }
        #endregion

        #region Errors
        private static void FatalError(Exception errorMessage)
        {
            Console.WriteLine("ERROR: General fatal error, attempting to save to CrashDump.json!");
            
            try
            {
                Utilities.UJson.PushCrashDump(errorMessage);
                Utilities.UJson.PushDownloadTally();
            }
            catch
            {
                Console.WriteLine("ERROR: Could not save to CrashDump.json/Push json failsafe, the cause of your error is a non-existant AppConfig.json file!");
            }

            Console.ReadLine();
        }
        #endregion
    }
}
