﻿using System;
using Utilities;

namespace RtenCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to RtenCore, if you are having issues, please refer to: 'https://gitlab.com/scalist/rten/wikis/home'\n");

            Install.StartInstall();

            if (UJson.PullShouldAutoDownload())
            {
                Console.WriteLine("NOTE: Auto-download is enabled & is starting now. You can press any key to exit the auto-install.");
                Install.InstallTimer();
            }
            else
            {
                Console.WriteLine("NOTE: Not auto-downloading as json config has advised against");
                Console.ReadKey();
            }
        }
    }
}
