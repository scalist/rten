﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace Utilities
{
    public static class UFile
    {
        public static void DownloadFromURL(string downloadLocation, string destinationName)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(downloadLocation, destinationName);
            }
        }

        public static void CreateZip(string fileLocation, string destinationName)
        {
            if (File.Exists(destinationName))
                ZipFile.CreateFromDirectory(fileLocation, destinationName);
        }

        public static void ExtractZip(string zipLocation, string extractLocation)
        {
            ZipFile.ExtractToDirectory(zipLocation, extractLocation);
        }

        public static void DeleteSpecifiedFile(string fileToDelete)
        {
            if (File.Exists(fileToDelete))
                File.Delete(fileToDelete);
            else
                Console.WriteLine("WARN: Could not delete file automatically. This is normal for first-time operation");
        }

        public static void DeleteWholeDir(string fileToDelete)
        {
            DirectoryInfo di = new DirectoryInfo(fileToDelete);

            if (di.Exists)
            {
                foreach (FileInfo file in di.EnumerateFiles())
                {
                    file.Delete();
                }

                foreach (DirectoryInfo dir in di.EnumerateDirectories())
                {
                    dir.Delete(true);
                }
            }
            else
                Console.WriteLine("WARN: Could not delete entire dir automatically. This is normal for first-time operation");
        }
    }
}
