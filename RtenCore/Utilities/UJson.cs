﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace Utilities
{
    public static class UJson
    {
        private static readonly string jsonLocation = string.Format("{0}AppConfig.json", AppDomain.CurrentDomain.BaseDirectory);
        private static readonly string jsonCrashLocation = string.Format("{0}CrashDump.json", AppDomain.CurrentDomain.BaseDirectory);

        #region Pulling
        private static string PullJson(bool shouldPullNormal)
        {
            if (shouldPullNormal)
            {
                using (StreamReader reader = new StreamReader(jsonLocation))
                    return JsonConvert.DeserializeObject(reader.ReadToEnd()).ToString();
            }
            else
            {
                using (StreamReader reader = new StreamReader(jsonCrashLocation))
                    return JsonConvert.DeserializeObject(reader.ReadToEnd()).ToString();
            }
        }

        public static string PullProjectName()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.ProjectName;
        }

        public static string PullDownloadURL()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.DownloadURL;
        }

        public static int PullDownloadTally()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.DownloadTally;
        }

        public static int PullHoursPerDownload()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.HoursPerDownload;
        }

        public static bool PullShouldAutoDownload()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.ShouldAutoDownload;
        }

        public static bool PullShouldAutoDelete()
        {
            dynamic jsonFile = JObject.Parse(PullJson(true));
            return jsonFile.ShouldAutoDelete;
        }

        public static string PullCrashDump()
        {
            dynamic jsonFile = JObject.Parse(PullJson(false));
            return string.Format("Project name: {0}\n\nError: {1}\n\nShouldAutoDownload: {2}\n\nShouldAutoDelete: {3}", jsonFile.ProjectName, jsonFile.CrashMessage, jsonFile.ShouldAutoDownload.ToString(), jsonFile.ShouldAutoDelete.ToString());
        }
        #endregion

        #region Pushing
        private class PushData
        {
            public string ProjectName { get; set; }
            public string DownloadURL { get; set; }
            public int DownloadTally { get; set; }
            public int HoursPerDownload { get; set; }
            public bool ShouldAutoDownload { get; set; }
            public bool ShouldAutoDelete { get; set; }
        }

        private class PushCrashData
        {
            public string ProjectName { get; set; }
            public string CrashMessage { get; set; }
            public bool ShouldAutoDownload { get; set; }
            public bool ShouldAutoDelete { get; set; }
        }

        public static void PushDownloadTally()
        {
            PushData pushDownloadTally = new PushData
            {
                ProjectName = PullProjectName(),
                DownloadURL = PullDownloadURL(),
                DownloadTally = PullDownloadTally() + 1,
                HoursPerDownload = PullHoursPerDownload(),
                ShouldAutoDownload = PullShouldAutoDownload(),
                ShouldAutoDelete = PullShouldAutoDelete()
            };

            File.WriteAllText(jsonLocation, JsonConvert.SerializeObject(pushDownloadTally));
        }

        public static void PushNew()
        {
            var pushData = new PushData
            {
                ProjectName = "Rten",
                DownloadURL = "https://docs.google.com/uc?export=download&id=1F06qo_6OBTqa0w6EaCySjHD60T1BdsMQ",
                DownloadTally = 1,
                HoursPerDownload = 2,
                ShouldAutoDownload = false,
                ShouldAutoDelete = true
            };

            File.WriteAllText(jsonLocation, JsonConvert.SerializeObject(pushData));
        }

        public static void PushCrashDump(Exception crashMessage)
        {
            var pushCrashData = new PushCrashData
            {
                ProjectName = PullProjectName(),
                CrashMessage = crashMessage.ToString(),
                ShouldAutoDownload = PullShouldAutoDownload(),
                ShouldAutoDelete = PullShouldAutoDelete()
            };

            File.WriteAllText(jsonCrashLocation, JsonConvert.SerializeObject(pushCrashData));
        }
        #endregion
    }
}
